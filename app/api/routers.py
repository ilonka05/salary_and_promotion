from fastapi import APIRouter

from app.api.endpoints import token_router, user_router


main_router = APIRouter()

main_router.include_router(token_router, prefix='/jwt', tags=['jwt'])
main_router.include_router(user_router, prefix='/users')
