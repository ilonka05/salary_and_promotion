from typing import Optional

from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import Field

from app.models import User


class UserFilter(Filter):
    """
    Фильтрация Сотрудников по дате повышения с учётом того,
    числится ли Сотрудник в штате.
    """
    order_by: Optional[list[str]] = Field(None,)
    is_staff__ilike: Optional[str] = Field(None,)

    class Constants(Filter.Constants):
        """Используемая модель и поле."""
        model = User
        search_model_fields = ['is_staff',]
