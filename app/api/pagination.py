from collections import namedtuple
from typing import Any, Sequence

from fastapi import Query, Response

from app.core.config import settings


class Pagination(namedtuple('Pagination', 'offset, limit')):
    __slots__ = ()

    @property
    def end(self):
        """Определение конечного индекса при разбивке."""
        return self.offset + self.limit


def get_pagination_params(
    offset: int = Query(settings.offset, ge=0),
    limit: int = Query(settings.limit, gt=0),
):
    """"Получение значений смещения и лимита."""
    return Pagination(offset, limit)


def paginated(
    data: Sequence[Any], paginator: Pagination,
) -> Sequence[Any]:
    """Получение среза последдовательности."""
    return data[paginator.offset:paginator.end]


def add_response_headers(
    response: Response,
    sequence: Sequence[Any],
    paginator: Pagination,
) -> Response:
    """Добавление заголовков к ответу."""
    response.headers['X-Total-Count'] = str(len(sequence))
    response.headers['X-Offset'] = str(paginator.offset)
    response.headers['X-Limit'] = str(paginator.limit)
    return response
