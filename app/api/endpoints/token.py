from fastapi import APIRouter, Depends

from app.core import utils_jwt
from app.core.user import validate_auth_user
from app.schemas.token import TokenInfo
from app.schemas.user import UserReadEmployee

router = APIRouter()


@router.post('/token/', response_model=TokenInfo)
def auth_user_issue_jwt(
    user: UserReadEmployee = Depends(validate_auth_user),
):
    """Функция выпуска токена для пользователя."""
    jwt_payload = {
        'sub': user.username,
        'username': user.username,
    }
    token = utils_jwt.encode_jwt(jwt_payload)
    return TokenInfo(
        access_token=token,
        token_type='Bearer',
    )
