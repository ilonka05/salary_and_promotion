from pydantic import BaseModel


class TokenInfo(BaseModel):
    """Схема представления QAuth2 токена."""
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """Схема данных, извлечённых из токена."""
    username: str | None = None
