import uvicorn
from fastapi import FastAPI

from app.api.routers import main_router
from app.core.config import settings

app = FastAPI(
    title=settings.app_title,
    description=settings.description
)

app.include_router(main_router)


def start():
    """Запуск проекта с помощью команды poetry run start."""
    uvicorn.run(
        'app.main:app',
        host='127.0.0.1',
        port=8000,
        reload=True
    )
