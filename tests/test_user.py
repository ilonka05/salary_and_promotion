from fastapi import status

from tests.fixtures.user import get_testuser, DATA_USER

NEW_USERNAME = 'testnewusername'
UPDATE_SCHEMA = {
    'username': NEW_USERNAME,
}
WRONG_NEW_USERNAME = DATA_USER['username']


class TestEmployee:
    async def test_get_me_correct_client(self, auth_client):
        """Тест получения Сотрудником информации о себе."""
        response = auth_client.get(
            '/users/me',
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном входе в систему, '
            'должен возвращаться статус-код 200.'
        )

    async def test_get_me_new_client(self, new_client):
        """Тест получения информации о себе, если Сотрудник не авторизован."""
        response = new_client.get(
            '/users/me',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Если Сотрудник не вошёл в систему, '
            'должен возвращаться статус-код 401.'
        )


class TestLeaderGet:
    async def test_get_all_superuser(self, auth_superuser, mock_users):
        """Тест получения Руководителем списка всех Сотрудников."""
        response = auth_superuser.get(
            '/users',
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном выводе списка всех Сотрудников, '
            'должен возвращаться статус-код 200.'
        )

    async def test_get_all_wronguser(self, new_client, mock_users):
        """
        Тест запрета получения списка всех Сотрудников,
        если Руководитель не вошёл в систему.
        """
        response = new_client.get(
            '/users',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Руководитель, не вошедший в систему, не может получить список, '
            'должен возвращаться статус-код 401.'
        )

    async def test_get_all_authuser(self, auth_client, mock_users):
        """
        Тест запрета получения списка всех Сотрудников,
        если запрос выполняет не Руководитель.
        """
        response = auth_client.get(
            '/users',
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN, (
            'Получить список всех Сотрудников может только Руководитель, '
            'иначе должен возвращаться статус-код 403.'
        )

    async def test_get_all_filter(self, auth_superuser, mock_users):
        """
        Тест работы фильтра (числится ли Сотрудник в штате),
        при получении списка всех Сотрудников.
        """
        response = auth_superuser.get(
            '/users/?is_staff__ilike=1',
        )
        assert len(response.json()) == 6, (
            'Количество Сотрудников в списке с использованием фильтра '
            'отличается от ожидаемого.'
        )

    async def test_get_all_pagination(self, auth_superuser, mock_users):
        """Тест работы пагинации при получении списка всех Сотрудников."""
        response = auth_superuser.get(
            '/users/?offset=1&limit=3',
        )
        result = response.json()
        assert len(result) == 3, (
            'Количество Сотрудников в списке с использованием пагинации '
            'отличается от ожидаемого.'
        )
        assert result[0]['id'] == 2, (
            'ID первого Сотрудника в списке с использованием пагинации '
            'отличается от ожидаемого.'
        )
        assert result[-1]['id'] == 4, (
            'ID последнего Сотрудника в списке с использованием пагинации '
            'отличается от ожидаемого.'
        )

    async def test_get_id_superuser(self, auth_superuser, mock_users):
        """Тест получения Руководителем данных Сотрудника по его ID."""
        response = auth_superuser.get(
            '/users/2',
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном выводе информации о Сотруднике, '
            'должен возвращаться статус-код 200.'
        )

    async def test_get_id_wronguser_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест получения Руководителем данных Сотрудника по его ID,
        если Сотрудника с указанным ID нет в БД.
        """
        response = auth_superuser.get(
            '/users/101',
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, (
            'Если Сотрудника с указанным ID нет в БД, '
            'должен возвращаться статус-код 404.'
        )

    async def test_get_id_wronguser(self, new_client, mock_users):
        """
        Тест запрета получения данных о Сотруднике,
        если Руководитель не вошёл в систему.
        """
        response = new_client.get(
            '/users/2',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Руководитель, не вошедший в систему, не может '
            'получить информацию, должен возвращаться статус-код 401.'
        )

    async def test_get_id_authuser(self, auth_client, mock_users):
        """
        Тест запрета получения данных о Сотруднике,
        если запрос не от Руководителя.
        """
        response = auth_client.get(
            '/users/2',
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN, (
            'Получить информацию о Сотруднике может только Руководитель, '
            'иначе должен возвращаться статус-код 403.'
        )


class TestLeaderPatch:
    async def test_patch_id_superuser(self, auth_superuser, mock_users):
        """Тест изменения Руководителем данных Сотрудника по его ID."""
        response = auth_superuser.patch(
            '/users/2',
            json=UPDATE_SCHEMA,
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном изменении информации о Сотруднике, '
            'должен возвращаться статус-код 200.'
        )

    async def test_patch_id_wrong_user_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест изменения Руководителем данных Сотрудника по его ID,
        если такого ID нет в БД.
        """
        response = auth_superuser.patch(
            '/users/101',
            json=UPDATE_SCHEMA,
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, (
            'Если Сотрудника с указанным ID нет в БД, '
            'должен возвращаться статус-код 404.'
        )

    async def test_patch_id_wrong_date_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест изменения Руководителем данных Сотрудника по его ID,
        если указана дата раньше дня запроса.
        """
        user_data = UPDATE_SCHEMA.copy()
        user_data['date_promotion'] = '2023-03-01'
        response = auth_superuser.patch(
            '/users/2',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Новая дата повышения не может быть раньше текущего дня, '
            'должен возвращаться статус-код 422.'
        )

    async def test_patch_id_wrong_salary_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест изменения Руководителем данных Сотрудника по его ID,
        если введённая сумма равна нулю или меньше.
        """
        user_data = UPDATE_SCHEMA.copy()
        user_data['salary'] = 0
        response = auth_superuser.patch(
            '/users/2',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Новая зарплата не может быть равна или меньше нуля, '
            'должен возвращаться статус-код 422.'
        )

    async def test_patch_id_wrong_username_and_password_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест изменения Руководителем данных Сотрудника по его ID,
        если указаны поля username, password, и их значения пусты.
        """
        user_data = UPDATE_SCHEMA.copy()
        user_data['username'] = ''
        user_data['password'] = ''
        response = auth_superuser.patch(
            '/users/2',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Новые значения username и password не могут быть пустыми, '
            'должен возвращаться статус-код 422.'
        )

    async def test_patch_id_not_is_staff_superuser(
        self, auth_superuser, register_client
    ):
        """
        Тест запрета изменений Руководителем данных Сотрудника по его ID,
        если Сотрудник уже не числится в штате.
        """
        response = auth_superuser.patch(
            '/users/2',
            json=UPDATE_SCHEMA,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Нельзя менять данные Сотрудника, если он уже не в штате, '
            'должен возвращаться статус-код 422.'
        )

    async def test_patch_id_wrong_username_superuser(
        self, auth_superuser, register_client,
    ):
        """
        Тест запрета изменений Руководителем данных Сотрудника по его ID,
        если новый логин уже есть в БД.
        """
        user_data = UPDATE_SCHEMA.copy()
        user_data['username'] = WRONG_NEW_USERNAME
        response = auth_superuser.patch(
            '/users/2',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Если новый логин уже есть в БД, '
            'должен возвращаться статус-код 422.'
        )

    async def test_patch_id_wronguser(self, new_client, mock_users):
        """
        Тест запрета редактирования данных о Сотрудника,
        если Руководитель не вошёл в систему.
        """
        response = new_client.patch(
            '/users/2',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Руководитель, не вошедший в систему, не может редактировать '
            'информацию, должен возвращаться статус-код 401.'
        )

    async def test_get_id_authuser(self, auth_client, mock_users):
        """
        Тест запрета редактирования данных Сотрудника,
        если запрос не от Руководителя.
        """
        response = auth_client.patch(
            '/users/2',
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN, (
            'Редактировать информацию о Сотруднике может '
            'только Руководитель, иначе должен возвращаться статус-код 403.'
        )


class TestLeaderPost:
    async def test_post_superuser(self, auth_superuser):
        """Тест добавления Руководителем данных Сотрудника в БД."""
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        response = auth_superuser.post(
            '/users',
            json=user_data,
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном добавлении информации о Сотруднике, '
            'должен возвращаться статус-код 200.'
        )

    async def test_post_wrong_date_superuser(self, auth_superuser):
        """
        Тест добавления Руководителем данных Сотрудника в БД,
        если указана дата раньше дня запроса.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2023-03-01'
        response = auth_superuser.post(
            '/users',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Дата повышения не может быть раньше текущего дня, '
            'должен возвращаться статус-код 422.'
        )

    async def test_post_wrong_salary_superuser(self, auth_superuser):
        """
        Тест добавления Руководителем данных Сотрудника в БД,
        если в поле зарплаты указана сумма, меньшая или равная нулю.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        user_data['salary'] = 0
        response = auth_superuser.post(
            '/users',
            json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Зарплата не может быть равна или меньше нуля, '
            'должен возвращаться статус-код 422.'
        )

    async def test_post_wrong_username_password_superuser(
        self, auth_superuser
    ):
        """
        Тест добавления Руководителем данных Сотрудника в БД,
        если значения полей username и password пусты.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        user_data['username'] = ''
        user_data['password'] = ''
        response = auth_superuser.post(
           '/users',
           json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Значения username и password не могут быть пустыми, '
            'должен возвращаться статус-код 422.'
        )

    async def test_post_wrong_data_superuser(self, auth_superuser):
        """
        Тест добавления Руководителем данных Сотрудника в БД
        с неверными данными.
        """
        response = auth_superuser.post(
           '/users',
           json={},
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Если при добавлении указаны неверные данные, '
            'должен возвращаться статус-код 422.'
        )

    async def test_post_wrong_username_superuser(
        self, auth_superuser, register_client
    ):
        """
        Тест запрета добавления Руководителем данных Сотрудника в БД,
        если указанный username уже есть в БД.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        user_data['username'] = WRONG_NEW_USERNAME
        response = auth_superuser.post(
           '/users',
           json=user_data,
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Если указанный username уже есть в БД, '
            'должен возвращаться статус-код 422.'
        )

    async def test_post_wronguser(self, new_client):
        """
        Тест запрета добавления данных Сотрудника в БД,
        если Руководитель не вошёл в систему.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        response = new_client.post(
           '/users',
           json=user_data,
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Руководитель, не вошедший в систему, не может добавлять '
            'информацию, должен возвращаться статус-код 401.'
        )

    async def test_post_authuser(self, auth_client):
        """
        Тест запрета добавления данных Сотрудника в БД,
        если запрос не от Руководителя.
        """
        user_data = DATA_USER.copy()
        user_data['date_promotion'] = '2035-03-01'
        response = auth_client.post(
           '/users',
           json=user_data,
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN, (
            'Добавлять информацию о Сотруднике может только '
            'Руководитель, иначе должен возвращаться статус-код 403.'
        )


class TestLeaderDelete:
    async def test_delete_id_superuser(self, auth_superuser, mock_users):
        """Тест запрета удаления Руководителем Сотрудника по его ID."""
        response = auth_superuser.delete(
           '/users/2',
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT, (
            'При успешном запрете удаления данных Сотрудника, '
            'должен возвращаться статус-код 204.'
        )

    async def test_delete_id_is_staff_superuser(
        self, auth_superuser, mock_users
    ):
        """Тест исключения из штата Руководством Сотрудника по его ID."""
        id = 2
        auth_superuser.delete(f'/users/{id}',)
        user = await get_testuser(user_id=id)
        assert user.is_staff == 0, (
            'При успешном исключении Сотрудника из штата, '
            'is_staff должен быть равен нулю.'
        )

    async def test_delete_id_wrong_user_superuser(
        self, auth_superuser, mock_users
    ):
        """
        Тест исключения Руководством Сотрудника из штата по его ID,
        если такого ID нет в БД.
        """
        response = auth_superuser.delete(
           '/users/101',
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND, (
            'Если Сотрудника с указанным ID нет в БД, '
            'должен возвращаться статус-код 404.'
        )

    async def test_delete_id_not_in_staff_superuser(
        self, auth_superuser, register_client
    ):
        """
        Тест запрета исключения Сотрудника из штата,
        если он уже в нём не состоит.
        """
        response = auth_superuser.delete('/users/2',)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'Если Сотрудник уже исключен из штата до выполнения запроса, '
            'должен возвращаться статус-код 422.'
        )

    async def test_delete_id_wrong_user(self, new_client, mock_users):
        """
        Тест запрета исключения Сотрудника из штата,
        если Руководитель не вошёл в систему.
        """
        response = new_client.delete(
           '/users/2',
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'Руководитель, не вошедший в систему, не может исключать '
            'Сотрудника из штата, должен возвращаться статус-код 401.'
        )

    async def test_delete_id_authuser(self, auth_client, mock_users):
        """
        Тест запрета исключения Сотрудника из штата,
        если запрос не от Руководителя.
        """
        response = auth_client.delete(
           '/users/2',
        )
        assert response.status_code == status.HTTP_403_FORBIDDEN, (
            'Исключать Сотрудника из штата может только Руководитель, '
            'иначе должен возвращаться статус-код 403.'
        )
