from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jwt.exceptions import InvalidTokenError

from app.core import utils_jwt
from app.schemas.user import UserReadEmployee


oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/jwt/token/',)


def get_current_token_payload(
    token: str = Depends(oauth2_scheme),
) -> UserReadEmployee:
    """Функция получения данных из валидного токена."""
    try:
        payload = utils_jwt.decode_jwt(
            token=token,
        )
    except InvalidTokenError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Неверный токен',
        )
    return payload
