import os
from pathlib import Path

from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict

BASE_DIR = Path(__file__).parent.parent.parent
ENV_FILE = os.path.join(BASE_DIR, 'infra', '.env')


class AuthJWT(BaseModel):
    """Настройки для аутентификации."""
    private_key_path: Path = BASE_DIR / 'certs' / 'jwt-private.pem'
    public_key_path: Path = BASE_DIR / 'certs' / 'jwt-public.pem'
    algorithm: str = 'RS256'
    access_token_expire_minutes: int = 15


class Settings(BaseSettings):
    """Базовые настройки приложения."""
    app_title: str = 'Сервис просмотра зарплаты'
    description: str = """
        Позволяет просмотреть Сотруднику свою текущую зарплату,
        по предоставленному руководством логину и паролю.\n
        Руководству доступны зарплаты всех Сотрудников,
        а также даты их повышения.
    """
    database_url: str = os.getenv('DATABASE_URL')
    auth_jwt: AuthJWT = AuthJWT()
    offset: int = 0
    limit: int = 10

    model_config = SettingsConfigDict(env_file=ENV_FILE)


settings = Settings()
