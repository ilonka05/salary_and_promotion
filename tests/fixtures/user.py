from datetime import date
from typing import AsyncGenerator

import pytest_asyncio
from fastapi import FastAPI, status
from fastapi.testclient import TestClient
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.core.db import get_async_session
from app.core.utils_jwt import hash_password
from app.models.user import User
from tests.conftest import AsyncSessionLocalTest

DATA_USER = {
    'username': 'testusername',
    'password': 'testpassword',
    'name': 'testname',
    'surname': 'testsurname',
    'salary': 80000,
    'date_promotion': date(2035, 3, 1),
}
DATA_ADMIN = {
    'username': 'admin',
    'password': 'admin',
    'name': 'adminname',
    'surname': 'adminsurname',
    'salary': 280000,
    'date_promotion': date(2034, 3, 1,),
}


async def get_testuser(
    user_id: int,
    db_session: AsyncSession = AsyncSessionLocalTest(),
):
    """Получение Сотрудника из тестовой БД по его ID."""
    user = await db_session.execute(
        select(User).filter(User.id == user_id)
    )
    user: User = user.scalars().first()
    return user


@pytest_asyncio.fixture
async def new_client(
    prepare_database: FastAPI,
    db_session: AsyncSession = AsyncSessionLocalTest(),
) -> AsyncGenerator | TestClient:
    """Фикстура создания нового клиента."""
    async def _get_test_db():
        yield db_session
    prepare_database.dependency_overrides[get_async_session] = _get_test_db
    with TestClient(prepare_database) as client:
        yield client


@pytest_asyncio.fixture
async def register_client(
    prepare_database: FastAPI,
    db_session: AsyncSession = AsyncSessionLocalTest(),
) -> AsyncGenerator:
    """Фикстура зарегистрированного клиента."""
    password = hash_password(DATA_USER['password'])
    register_user = User(
        username=DATA_USER['username'],
        password=password,
        name=DATA_USER['name'],
        surname=DATA_USER['surname'],
        salary=DATA_USER['salary'],
        date_promotion=DATA_USER['date_promotion'],
        is_staff=0,
    )
    db_session.add(register_user)
    await db_session.commit()
    await db_session.refresh(register_user)
    yield register_user


@pytest_asyncio.fixture
async def auth_client(
    new_client,
    register_client,
) -> AsyncGenerator | TestClient:
    """Фикстура для клиента, вошедшего в систему."""
    response = new_client.post(
        '/jwt/token',
        data={
            'username': DATA_USER['username'],
            'password': DATA_USER['password'],
        }
    )
    assert response.status_code == status.HTTP_200_OK
    access_token = response.json().get('access_token')
    new_client.headers.update({'Authorization': f'Bearer {access_token}'})
    yield new_client


@pytest_asyncio.fixture
async def superuser(
    db_session: AsyncSession = AsyncSessionLocalTest(),
) -> AsyncGenerator | TestClient:
    """Фикстура Руководителя."""
    password = hash_password(DATA_ADMIN['password'])
    superuser = User(
        username=DATA_ADMIN['username'],
        password=password,
        name=DATA_ADMIN['name'],
        surname=DATA_ADMIN['surname'],
        salary=DATA_ADMIN['salary'],
        date_promotion=DATA_ADMIN['date_promotion'],
        is_superuser=True,
    )
    db_session.add(superuser)
    await db_session.commit()
    await db_session.refresh(superuser)
    yield superuser


@pytest_asyncio.fixture
async def auth_superuser(
    new_client,
    superuser,
) -> AsyncGenerator | TestClient:
    """Фикстура для Руководителя, вошедшего в систему."""
    response = new_client.post(
        '/jwt/token',
        data={
            'username': DATA_ADMIN['username'],
            'password': DATA_ADMIN['password'],
        }
    )
    assert response.status_code == status.HTTP_200_OK
    access_token = response.json().get('access_token')
    new_client.headers.update({'Authorization': f'Bearer {access_token}'})
    yield new_client


@pytest_asyncio.fixture
async def mock_users(
    db_session: AsyncSession = AsyncSessionLocalTest(),
) -> AsyncGenerator:
    """Фикстура заполнения БД Сотрудниками."""
    password = hash_password(DATA_USER['password'])
    mock_users = [
        User(
            username=f'{DATA_USER["username"]}_{i}',
            password=password,
            name=f'{DATA_USER["name"]}_{i}',
            surname=f'{DATA_USER["surname"]}_{i}',
            salary=f'{DATA_USER["salary"] + i}',
            date_promotion=DATA_USER['date_promotion'],
        ) for i in range(1, 6)
    ]
    db_session.add_all(mock_users)
    await db_session.commit()
    mock_users = await db_session.execute(select(User))
    mock_users = mock_users.scalars().all()
    await db_session.commit()
