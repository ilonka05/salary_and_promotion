from datetime import date

from sqlalchemy import Boolean, Column, Date, Integer, String

from app.core.db import Base


class User(Base):
    """Модель сотрудника."""
    id: int = Column(Integer, primary_key=True)
    username: str = Column(String, nullable=False, unique=True)
    password: bytes = Column(String, nullable=False)
    name: str = Column(String, nullable=False)
    surname: str = Column(String, nullable=False)
    salary: int = Column(Integer, nullable=False)
    date_promotion: date = Column(Date, nullable=False)
    is_staff: bool = Column(Boolean, default=True)
    is_active: bool = Column(Boolean, default=True)
    is_superuser: bool = Column(Boolean, default=False)
