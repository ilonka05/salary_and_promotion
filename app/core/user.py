from fastapi import Depends, Form, HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession

from app.core import utils_jwt
from app.core.db import get_async_session
from app.core.token import get_current_token_payload
from app.crud.user import user_crud
from app.schemas.user import UserBase


async def validate_auth_user(
    username: str = Form(),
    password: str = Form(),
    session: AsyncSession = Depends(get_async_session),
):
    """Функция аутентификации пользователя."""
    unauthed_exc = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Неверный логин или пароль',
    )
    user = await user_crud.get_user_by_username(
        username=username.strip(),
        session=session,
    )
    if not user:
        raise unauthed_exc

    if not utils_jwt.validate_password(
        password=password.strip(),
        hashed_password=user.password,
    ):
        raise unauthed_exc

    user = await get_current_active_user(
        user=user, session=session,
    )

    return user


async def get_current_user(
    payload: dict = Depends(get_current_token_payload),
    session: AsyncSession = Depends(get_async_session),
) -> UserBase:
    """Функция возвращения Сотрудника, если объект в токене есть в БД."""
    username: str | None = payload.get('sub')
    user = await user_crud.get_user_by_username(
        username=username,
        session=session,
    )
    if user:
        return user
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Некорректный токен',
    )


async def get_current_active_user(
    user: UserBase = Depends(get_current_user),
    session: AsyncSession = Depends(get_async_session),
):
    """Функция возвращения Сотрудника, если он активен."""
    user = await user_crud.get_obj_by_name(
        username=user.username,
        session=session,
    )
    if user.is_active:
        return user
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail='Пользователь не активен',
    )


async def get_current_superuser(
    current_superuser: UserBase = Depends(get_current_active_user),
    session: AsyncSession = Depends(get_async_session),
):
    """Функция проверки, является ли пользователь Руководителем."""
    current_superuser = await user_crud.get_obj_by_name(
        username=current_superuser.username,
        session=session,
    )
    if current_superuser.is_superuser:
        return current_superuser
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail='У вас нет прав Руководителя',
    )
