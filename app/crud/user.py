from fastapi import Depends
from fastapi_filter import FilterDepends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.filters import UserFilter
from app.core.db import get_async_session
from app.crud.base import CRUDBase
from app.models import User


class CRUDUser(CRUDBase):

    async def get_user_by_username(
        self,
        username: str,
        session: AsyncSession,
    ):
        """Функция получения данных Сотрудника из БД по его логину."""
        db_obj = await session.execute(
            select(User).where(User.username == username)
        )
        return db_obj.scalars().first()

    async def user_not_staff(
        self,
        user: User,
        session: AsyncSession,
    ):
        """Исключить Сотрудника из штата."""
        user.is_staff = False
        session.add(user)
        await session.commit()
        await session.refresh(user)
        return user

    async def get_user_filter(
        self,
        user_filter: UserFilter = FilterDepends(UserFilter),
        session: AsyncSession = Depends(get_async_session),
    ) -> list:
        """Функция фильтрации по полям модели User."""
        query = select(User)
        query = user_filter.filter(query)
        query = user_filter.sort(query)
        result = await session.execute(query)
        return result.scalars().all()


user_crud = CRUDUser(User)
