from fastapi import APIRouter, Depends, HTTPException, Response, status
from fastapi_filter import FilterDepends
from sqlalchemy.ext.asyncio import AsyncSession

from app.api.filters import UserFilter
from app.api.pagination import (Pagination, add_response_headers,
                                get_pagination_params, paginated)
from app.api.validators import (check_name_duplicate, check_obj_exists,
                                user_is_staff)
from app.core.db import get_async_session
from app.core.user import get_current_active_user, get_current_superuser
from app.crud.user import user_crud
from app.schemas.user import (UserBase, UserCreate, UserReadEmployee,
                              UserReadLeader, UserUpdate)

router = APIRouter()


@router.get(
    '/me',
    tags=['employee'],
    response_model=UserReadEmployee,
    dependencies=[Depends(get_current_active_user)],
)
async def read_users_me_info(
    user: UserBase = Depends(get_current_active_user),
):
    """Получение информации Сотрудником, вошедшего в систему."""
    return user


@router.get(
    '/',
    tags=['leader'],
    response_model=list[UserReadLeader],
    dependencies=[Depends(get_current_superuser)],
)
async def get_all_users(
    response: Response,
    session: AsyncSession = Depends(get_async_session),
    user_filter: UserFilter = FilterDepends(UserFilter),
    pagination: Pagination = Depends(get_pagination_params),
) -> list[UserReadLeader]:
    """
    Только для Руководителей.

    Получение списка всех Сотрудников.

    Доступна сортировка, для этого укажите интересуемое поле,
    например, без кавычек 'date_promotion'.
    Сортировка происходит по принципу от меньшего к большему.

    Если нужно отобразить всех Сотрудников в штате, в поле
    is_staff__ilike указать без кавычек '1'.

    Если нужно отобразить всех Сотрудников, которые уже не числятся в штате,
    в поле is_staff__ilike указать без кавычек '0'.

    Реализована пагинация страниц.
    Эта стратегия использует два параметра: offset и limit.
    offset указывает, сколько элементов нужно пропустить с начала,
    limit указывает, сколько элементов нужно вернуть после пропуска.
    """
    users = await user_crud.get_user_filter(
        user_filter=user_filter,
        session=session,
    )
    response = add_response_headers(
        response, users, pagination,
    )
    return paginated(users, pagination)


@router.get(
    '/{user_id}',
    tags=['leader'],
    response_model=UserReadLeader,
    dependencies=[Depends(get_current_superuser)],
)
async def get_user(
    user_id: int,
    session: AsyncSession = Depends(get_async_session),
) -> UserReadLeader:
    """
    Только для Руководителей.

    Получение данных о Сотруднике по его ID.
    """
    return await check_obj_exists(
        obj_id=user_id,
        crud=user_crud,
        session=session
    )


@router.patch(
    '/{user_id}',
    tags=['leader'],
    response_model=UserReadLeader,
    dependencies=[Depends(get_current_superuser)],
)
async def update_user(
    user_id: int,
    obj_in: UserUpdate,
    session: AsyncSession = Depends(get_async_session),
) -> UserReadLeader:
    """
    Только для Руководителей.

    Редактирование данных Сотрудника.
    Оставьте только необходимые для редактирования поля.


    Нельзя менять данные Сотрудника, который не числится в штате.
    """
    user = await check_obj_exists(
        obj_id=user_id,
        crud=user_crud,
        session=session,
    )
    if not user.is_staff:
        user_is_staff(
            is_staff=user.is_staff
        )
    if obj_in.username is not None:
        await check_name_duplicate(
            username=obj_in.username,
            crud=user_crud,
            session=session,
        )
    user = await user_crud.update(
        db_obj=user,
        obj_in=obj_in,
        session=session,
    )
    return user


@router.post(
    '/',
    tags=['leader'],
    response_model=UserReadLeader,
    dependencies=[Depends(get_current_superuser)],
)
async def create_user(
    user: UserCreate,
    session: AsyncSession = Depends(get_async_session),
) -> UserReadLeader:
    """
    Только для Руководителей.

    Добавление нового Сотрудника в БД.
    """
    await check_name_duplicate(
        username=user.username, crud=user_crud, session=session
    )
    return await user_crud.create(obj_in=user, session=session)


@router.delete(
    '/{user_id}',
    tags=['leader'],
    dependencies=[Depends(get_current_superuser)],
    status_code=status.HTTP_204_NO_CONTENT,
)
async def delete_user(
    user_id: int,
    session: AsyncSession = Depends(get_async_session),
) -> None:
    """
    Только для Руководителей.

    Нельзя удалить Сотрудника, можно лишь исключить его из штата.
    """
    user = await check_obj_exists(
        obj_id=user_id,
        crud=user_crud,
        session=session,
    )
    if not user.is_staff:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail='Данный Сотрудник уже не числится в штате'
        )
    user = await user_crud.user_not_staff(
        user=user,
        session=session,
    )
