from fastapi import status

from tests.fixtures.user import DATA_USER


WRONG_USERNAME = 'wrongusername'
WRONG_PASSWORD = 'wrongpassword'


class TestLogin:
    async def test_login_correct_client(self, new_client, register_client):
        """Тест входа в систему с корректными данными."""
        response = new_client.post(
            '/jwt/token/',
            data={
                'username': DATA_USER['username'],
                'password': DATA_USER['password'],
            },
        )
        assert response.status_code == status.HTTP_200_OK, (
            'При успешном входе в систему, '
            'должен возвращаться статус-код 200.'
        )

    async def test_login_wrong_client(self, new_client):
        """Тест входа в систему с неверными данными."""
        response = new_client.post(
            '/jwt/token/',
            data={'username': WRONG_USERNAME, 'password': WRONG_PASSWORD},
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED, (
            'При попытке входа в систему с данными, отсутствующими в БД, '
            'должен возвращаться статус-код 401.'
        )

    async def test_wrong_username(self, new_client, register_client):
        """Тест входа в систему с неверным логином."""
        response = new_client.post(
            '/jwt/token/',
            data={
                'username': WRONG_USERNAME,
                'password': DATA_USER['password'],
            },
        )
        assert response.json().get('username') != register_client.username, (
            'Нельзя войти в систему по логину, которого нет в БД. '
        )

    async def test_wrong_password(self, new_client, register_client):
        """Тест входа в систему с неверным паролем."""
        response = new_client.post(
            '/jwt/token/',
            data={
                'username': DATA_USER['username'],
                'password': WRONG_PASSWORD,
            },
        )
        assert response.json().get('password') != register_client.password, (
            'Нельзя войти в систему по паролю, которого нет в БД. '
        )

    async def test_invalid_data(self, new_client, register_client):
        """Тест входа в систему с недопустимыми данными."""
        response = new_client.post(
            '/jwt/token/',
            data={},
        )
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY, (
            'При попытке входа в систему с недопустимыми данными '
            'должен возвращаться статус-код 422.'
        )
