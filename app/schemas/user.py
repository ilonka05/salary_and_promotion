from datetime import date, timedelta
from typing import Optional

from pydantic import (BaseModel, Field, PositiveInt,
                      field_validator)


class UserBase(BaseModel):
    """Базовая схема Сотрудника."""
    username: Optional[str] = Field(None,)
    name: Optional[str] = Field(None,)
    surname: Optional[str] = Field(None,)
    salary: Optional[PositiveInt] = Field(1,)
    is_staff: Optional[bool] = Field(True,)
    is_superuser: Optional[bool] = Field(False,)


class UserCreate(UserBase):
    """Схема занесения Сотрудника в БД."""
    username: str
    password: str
    name: str
    surname: str
    salary: PositiveInt = Field(1,)
    date_promotion: date = Field(date.today() + timedelta(days=7))
    is_staff: bool = True
    is_superuser: bool = False

    @field_validator('date_promotion')
    def check_date_promotion_later_than_now(cls, value):
        if value <= date.today():
            raise ValueError(
                'Дата повышения '
                'не может быть меньше текущей даты'
            )
        return value

    @field_validator('username', 'password')
    def field_cannot_be_null(cls, value: str):
        """Проверка полей на то, что они не равны None и не пусты."""
        if value is None or value.strip() == '':
            raise ValueError(
                'Поля "username" и "password" не могут быть пустыми'
            )
        return value


class UserReadEmployee(UserBase):
    """Схема просмотра данных Сотрудником."""
    pass


class UserReadLeader(UserBase):
    """Схема просмотра данных Руководителем."""
    id: int
    password: Optional[str] = Field(None,)
    date_promotion: Optional[date] = Field(date.today() + timedelta(days=7))


class UserUpdate(UserBase):
    """Схема обновления данных о Сотруднике."""
    password: Optional[str] = Field(None,)
    date_promotion: Optional[date] = Field(date.today() + timedelta(days=7))

    @field_validator('date_promotion')
    def check_date_promotion_later_than_now(cls, value):
        if value <= date.today():
            raise ValueError(
                'Дата повышения '
                'не может быть меньше текущей даты'
            )
        return value

    @field_validator('username', 'password')
    def field_cannot_be_null(cls, value: str):
        """Проверка полей на то, что они не равны None и не пусты."""
        if value is None or value.strip() == '':
            raise ValueError(
                'Поля "username" и "password" не могут быть пустыми'
            )
        return value
