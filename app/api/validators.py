from fastapi import HTTPException, status
from sqlalchemy.ext.asyncio import AsyncSession


async def check_obj_exists(
    obj_id: int,
    crud,
    session: AsyncSession,
):
    """Проверка, есть ли объект в БД."""
    obj = await crud.get(obj_id, session)
    if obj is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='Сотрудника с указанным ID нет в БД',
        )
    return obj


async def check_name_duplicate(
    username: str,
    crud,
    session: AsyncSession,
) -> None:
    """Проверка, есть ли уже Сотрудник с таким логином."""
    obj = await crud.get_obj_by_name(
        username=username,
        session=session,
    )
    if obj is not None:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail='Сотрудник с таким логином уже существует',
        )


def user_is_staff(
    is_staff: bool,
) -> None:
    """Проверка, числится ли Сотрудник в штате."""
    if not is_staff:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail='Нельзя менять данные Сотрудника, '
            'который уже не числится в штате.'
        )
